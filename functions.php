<?php
/**
 * Horsesales functions and definitions.
 *
 * @package Horsesales
 */

/**
 * Main setup function
 *
 * Sets up theme defaults and registers support for various WordPress features.
 */
function horsesales_setup() {
	// Make theme available for translation.
	load_theme_textdomain( 'horsesales', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Let WordPress manage the document title.
	add_theme_support( 'title-tag' );

	// Enable support for Post Thumbnails on posts and pages.
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'primary' => esc_html__( 'Primary', 'horsesales' ),
		)
	);

	// Switch default core markup to output valid HTML5.
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

}
add_action( 'after_setup_theme', 'horsesales_setup' );

/**
 * Enqueue stylesheets.
 */
function horsesales_css() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/lib/css/bootstrap.min.css', array(), '4.3.1' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/lib/css/font-awesome.min.css', array(), '4.7.0' );
	wp_enqueue_style( 'lato', '//fonts.googleapis.com/css?family=Lato:400,400i,700,900', array(), '1.0.0' );
	wp_enqueue_style( 'lity', get_template_directory_uri() . '/lib/css/lity.min.css', array(), '2.3.1' );
	wp_enqueue_style( 'aos', get_template_directory_uri() . '/lib/css/aos.min.css', array(), '2.3.4' );
	wp_enqueue_style( 'horsesales', get_stylesheet_uri(), array( 'bootstrap' ), wp_get_theme()->get( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'horsesales_css' );

/**
 * Enqueue front-end scripts.
 */
function horsesales_js() {
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/lib/js/bootstrap.bundle.min.js', array( 'jquery' ), '4.3.1', true );
	wp_enqueue_script( 'aos', get_template_directory_uri() . '/lib/js/aos.min.js', array(), '2.3.4', true );
	wp_enqueue_script( 'lity', get_template_directory_uri() . '/lib/js/lity.min.js', array( 'jquery' ), '2.3.1', true );
	wp_enqueue_script( 'general', get_template_directory_uri() . '/js/general.js', array( 'aos' ), wp_get_theme()->get( 'Version' ), true );

	// Conditionally load woocommerce/auction customisation scripts.
	if ( is_product() ) {
		wp_enqueue_script( 'woocommerce-custom', get_template_directory_uri() . '/js/woocommerce-custom.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true );
		global $post;
		$product = wc_get_product( $post->ID );
		if ( $product->get_type() === 'auction' ) {
			wp_enqueue_script( 'auctions-custom', get_template_directory_uri() . '/js/auctions-custom.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'horsesales_js' );

/**
 * Enqueue admin scripts.
 */
function horsesales_admin_styles() {
	wp_enqueue_media();
	wp_enqueue_script( 'media-upload-custom', get_template_directory_uri() . '/js/media-uploader.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_style( 'horsesales-admin-styles', get_template_directory_uri() . '/css/admin.css', array(), wp_get_theme()->get( 'Version' ) );
}
add_action( 'admin_enqueue_scripts', 'horsesales_admin_styles' );

/**
 * Register widget area.
 */
function horsesales_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'horsesales' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'horsesales' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'horsesales_widgets_init' );

/**
 * Remove unnecessary admin menus.
 *
 * For users with less than admin privileges, remove other menus too.
 */
function horsesales_remove_menus() {
	remove_menu_page( 'edit-comments.php' );

	if ( ! current_user_can( 'administrator' ) ) {
		remove_menu_page( 'index.php' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'themes.php' );
		remove_menu_page( 'plugins.php' );
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'options-general.php' );
		remove_menu_page( 'wpcf7' );
	}
}
add_action( 'admin_menu', 'horsesales_remove_menus' );

/**
 * Show all service post types on the archive page.
 *
 * @param object $query A WP Query object.
 */
function hs_show_all_services( $query ) {
	if ( ! is_admin() && $query->is_main_query() ) {

		if ( is_post_type_archive( 'service' ) ) {
			$query->set( 'posts_per_page', -1 );
		}
	}
}
add_action( 'pre_get_posts', 'hs_show_all_services' );

/**
 * Disable the WP Heartbeat API.
 */
function horsesales_stop_heartbeat() {
	wp_deregister_script( 'heartbeat' );
}
add_action( 'init', 'horsesales_stop_heartbeat', 1 );

/**
 * Change the default excerpt length to 20 words.
 */
function horsesales_excerpt_length() {
	return 12;
}
add_filter( 'excerpt_length', 'horsesales_excerpt_length', 999 );

/**
 * Don't show the admin bar.
 */
add_filter( 'show_admin_bar', '__return_false' );

/**
 * Load woocommerce customisation file.
 */
include get_template_directory() . '/inc/woocommerce.php';

/**
 * Load auction customisation file.
 */
include get_template_directory() . '/inc/auctions-custom.php';

/**
 * Load horse meta boxes file.
 */
include get_template_directory() . '/inc/horse-meta-boxes.php';

/**
 * Load theme customizer.
 */
include get_template_directory() . '/inc/customizer.php';

/**
 * Instantiate the custom post types and their fields.
 */
include get_template_directory() . '/classes/class-post-type-service.php';
$service = new Service();
