<?php
/**
 * The template for displaying all single posts.
 *
 * @package Horsesales
 */

get_header();

get_template_part( 'template-parts/breadcrumbs' );

?>

<div id="primary" class="content-area container" data-aos="fade-up">

	<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile; // End of the loop.
		?>

		<!-- clears any woocommerce content that doesn't have tabs -->
		<div style="clear:both"></div>

	</main>

</div><!-- #primary -->

<?php get_footer(); ?>
