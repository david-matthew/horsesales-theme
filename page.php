<?php
/**
 * The template for displaying all pages by default.
 *
 * @package Horsesales
 */

get_header();

get_template_part( 'template-parts/breadcrumbs' );

get_template_part( 'template-parts/page-banner' );

?>

<div id="primary" class="content-area container" data-aos="fade-up">

	<main id="main" class="site-main">

		<?php

		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

		endwhile;
		?>

	</main>

</div><!-- #primary -->

<?php get_footer(); ?>
