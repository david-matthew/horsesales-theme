<?php
/**
 * Template Name: Slim Page
 *
 * A slightly modified version of the default page template.
 *
 * @package Horsesales
 */

get_header();

get_template_part( 'template-parts/breadcrumbs' );

get_template_part( 'template-parts/page-banner' );

?>

<div id="primary" class="content-area container" data-aos="fade-up">

	<main id="main" class="site-main">

		<div class="col-xs-12 col-lg-6 offset-lg-3">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile;
		?>

		</div>

	</main>

</div><!-- #primary -->

<?php get_footer(); ?>
