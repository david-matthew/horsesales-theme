# Horsesales Theme
A custom Wordpress theme for the [horsesales.ie](https://www.horsesales.ie) horse auction website, integrated with Woocommerce.

## Dependencies
#### Plugins
* Ultimate Member
* Woocommerce
* Woocommerce Simple Auctions
* Yoast SEO

#### Libraries
* Bootstrap 4.3.1
* Animate On Scroll 2.3.4
* Lity 2.3.1

## Coding Standards
This theme uses the Wordpress Coding Standards, as defined [here](https://make.wordpress.org/core/handbook/best-practices/coding-standards/).
