<?php
/**
 * The template for displaying the footer
 *
 * @package Horsesales
 */

?>

<footer>

	<div class="container">

		<div class="row">

			<div class="col-md-4 footer-section">

				<h3>Links</h3>

				<ul>

					<li><a href="<?php echo esc_url( get_theme_mod( 'ft_url_1' ) ); ?>"><?php echo esc_html( get_theme_mod( 'ft_txt_1' ) ); ?></a></li>

					<li><a href="<?php echo esc_url( get_theme_mod( 'ft_url_2' ) ); ?>"><?php echo esc_html( get_theme_mod( 'ft_txt_2' ) ); ?></a></li>

					<li><a href="<?php echo esc_url( get_theme_mod( 'ft_url_3' ) ); ?>"><?php echo esc_html( get_theme_mod( 'ft_txt_3' ) ); ?></a></li>

					<li><a href="<?php echo esc_url( get_theme_mod( 'ft_url_4' ) ); ?>"><?php echo esc_html( get_theme_mod( 'ft_txt_4' ) ); ?></a></li>

				</ul>

			</div>

			<div class="col-md-4 footer-section">

				<h3>Contact Details</h3>

				<ul>

					<li><i class="fa fa-building-o"></i>11 The Crescent, O'Connell Street, Limerick, Ireland</li>

					<li><i class="fa fa-phone"></i><a href="tel:0035361278601">061278601</a> / <a href="tel:00353864056617">0864056617</a> </li>

					<li><i class="fa fa-envelope-o"></i><a href="<?php echo esc_url( get_theme_mod( 'contact_url' ) ); ?>">Send us a Message</a></li>

				</ul>

			</div>

			<div class="col-md-4 footer-section">

				<h3>Follow Us</h3>

				<p>For our latest social media activity and videos, make sure to follow us on the below channels:</p>

				<div class="social-links">

					<a href="//instagram.com/horsesalesie" target="_blank"><i class="fa fa-instagram"></i></a>

					<a href="//facebook.com/horsesalesie" target="_blank"><i class="fa fa-facebook"></i></a>

					<a href="//twitter.com/horsesalesie" target="_blank"><i class="fa fa-twitter"></i></a>

					<a href="//linkedin.com/company/horsesalesie/" target="_blank"><i class="fa fa-linkedin"></i></a>

					<a href="//youtube.com/channel/UCg028ITvq_rk2t5cTJDKLNA" target="_blank"><i class="fa fa-youtube"></i></a>

				</div>

			</div>

		</div>

	</div>

	<div class="sub-footer">  

		<div class="container">

			<span class="copyright">&copy; HorseSales.ie <?php echo esc_html( date( 'Y' ) ); ?> | All Rights Reserved.</span>

		</div>

	</div>      

</footer>

<?php wp_footer(); ?>

</body>

</html>
