<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Horsesales
 */

get_header();

get_template_part( 'template-parts/breadcrumbs' );

?>

<div id="primary" class="content-area container" data-aos="fade-up">

	<main id="main" class="site-main">

		<section class="error-404 not-found">

			<header class="page-header">

				<h1 class="entry-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'horsesales' ); ?></h1>

			</header>

			<div class="page-content">

				<p><?php esc_html_e( 'Sorry, but nothing was found at this location.', 'horsesales' ); ?></p>

			</div><!-- .page-content -->

		</section><!-- .error-404 -->

	</main>

</div><!-- #primary -->

<?php get_footer(); ?>
