<?php
/**
 * Template Name: Marketplace
 * 
 * The template for displaying the marketplace categories.
 *
 * @package Horsesales
 */

get_header();

get_template_part( 'template-parts/breadcrumbs' );

get_template_part( 'template-parts/page-banner' );

?>

<div id="primary" class="content-area container" data-aos="fade-up">

	<main id="main" class="site-main">

		<div class="row text-center">

			<?php get_template_part( 'template-parts/search-service' ); ?>

		</div><!-- row -->

		<div class="row mkt-placeholders">

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_1' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_1' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_1' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_2' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_2' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_2' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_3' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_3' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_3' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_4' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_4' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_4' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_5' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_5' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_5' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_6' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_6' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_6' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_7' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_7' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_7' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_8' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_8' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_8' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_9' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_9' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_9' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_10' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_10' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_10' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_11' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_11' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_11' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_12' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_12' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_12' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_13' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_13' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_13' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

			<div class="col-6 col-md-4 col-lg-3 plc-wrap">

				<div class="plc-inner">

					<a href="<?php echo esc_url( get_theme_mod( 'plc_cat_url_14' ) ); ?>">

						<div class="plc-img-wrap">

							<div class="plc-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'plc_img_url_14' ) ); ?>"></div>

							<i class="plc-view center-absolute fa fa-search" aria-hidden="true"></i>

							<div class="plc-title-wrap text-center">

								<h3 class="plc-title center-absolute"><?php echo esc_html( get_theme_mod( 'plc_title_14' ) ); ?></h3>

							</div>

						</div>

					</a>

				</div>

			</div>

		</div><!-- mkt-placeholders -->

		<?php get_template_part( 'template-parts/cta-service' ); ?>

	</main>

</div><!-- #primary -->

<?php get_footer(); ?>
