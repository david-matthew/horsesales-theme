<?php
/**
 * The template for displaying the homepage
 *
 * @package Horsesales
 */

get_header();

?>

<div id="home-carousel" class="carousel slide carousel-fade" data-ride="carousel">

	<div class="carousel-header">

		<img src="<?php echo esc_url( get_template_directory_uri() . '/img/header-the-future-of-equine-sales.svg' ); ?>" alt="Horsesales.ie - The Future of Equine Sales">

	</div>

	<div class="carousel-item active" data-interval="6000">

		<div class="bg-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'c_img_url_1' ) ); ?>')">

			<div class="overlay"></div>

		</div>

		<div class="overlay"></div>

		<div class="carousel-message">

			<p><?php echo esc_html( get_theme_mod( 'c_main_txt_1' ) ); ?></p>

			<a href="<?php echo esc_url( get_theme_mod( 'c_btn_url_1' ) ); ?>"><?php echo esc_html( get_theme_mod( 'c_btn_txt_1' ) ); ?></a>

		</div>

	</div>

	<div class="carousel-item" data-interval="6000">

		<div class="bg-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'c_img_url_2' ) ); ?>')">

			<div class="overlay"></div>

		</div>

		<div class="overlay"></div>

		<div class="carousel-message">

			<p><?php echo esc_html( get_theme_mod( 'c_main_txt_2' ) ); ?></p>

			<a href="<?php echo esc_url( get_theme_mod( 'c_btn_url_2' ) ); ?>"><?php echo esc_html( get_theme_mod( 'c_btn_txt_2' ) ); ?></a>

		</div>

	</div>

	<div class="carousel-item" data-interval="6000">

		<div class="bg-img" style="background-image: url('<?php echo esc_url( get_theme_mod( 'c_img_url_3' ) ); ?>')">

			<div class="overlay"></div>

		</div>

		<div class="overlay"></div>

		<div class="carousel-message">

			<p><?php echo esc_html( get_theme_mod( 'c_main_txt_3' ) ); ?></p>

			<a href="<?php echo esc_url( get_theme_mod( 'c_btn_url_3' ) ); ?>"><?php echo esc_html( get_theme_mod( 'c_btn_txt_3' ) ); ?></a>

		</div>

	</div>

	<ol class="carousel-indicators">

		<li data-target="#home-carousel" data-slide-to="0" class="active"></li>

		<li data-target="#home-carousel" data-slide-to="1"></li>

		<li data-target="#home-carousel" data-slide-to="2"></li>

	</ol>

	<div class="pointer-divider">

		<div></div>

		<img src="<?php echo esc_html( get_template_directory_uri() . '/img/pointer-divider.svg' ); ?>" alt="pointer">

		<div></div>

	</div>

</div><!-- end carousel -->

<!-- ad space -->
<div class="container">

	<?php

	if ( get_theme_mod( 'ads_pro_home_desktop' ) ) {
		echo do_shortcode( get_theme_mod( 'ads_pro_home_desktop' ) );
	}

	if ( get_theme_mod( 'ads_pro_home_tablet' ) ) {
		echo do_shortcode( get_theme_mod( 'ads_pro_home_tablet' ) );
	}

	if ( get_theme_mod( 'ads_pro_home_mobile' ) ) {
		echo do_shortcode( get_theme_mod( 'ads_pro_home_mobile' ) );
	}

	?>

</div>

<!-- our services -->
<div id="services" class="container">

	<h1 class="entry-title"><?php echo esc_html( get_theme_mod( 's_heading' ) ); ?></h1>

	<div class="row">

		<div id="service-1" class="col-md-6 col-xl-4 service" data-aos="fade-up">

			<img class="img-fluid" src="<?php echo esc_url( get_theme_mod( 's_img_url_1' ) ); ?>" alt="<?php echo esc_html( get_theme_mod( 's_h3_txt_1' ) ); ?>">

			<div class="service-details-wrap">

				<div class="service-details">

					<i class="fa <?php echo esc_attr( get_theme_mod( 's_icon_1' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 's_h3_txt_1' ) ); ?></h3>

					<i class="fa fa-caret-up" aria-hidden="true"></i>

					<span></span>

					<div class="service-desc"><?php echo wp_kses_post( get_theme_mod( 's_content_1' ) ); ?></div>

				</div>

			</div>

		</div>

		<div id="service-2" class="col-md-6 col-xl-4 service" data-aos="fade-up" data-aos-delay="200">

			<img class="img-fluid" src="<?php echo esc_url( get_theme_mod( 's_img_url_2' ) ); ?>" alt="<?php echo esc_html( get_theme_mod( 's_h3_txt_2' ) ); ?>">

			<div class="service-details-wrap">

				<div class="service-details">

					<i class="fa <?php echo esc_attr( get_theme_mod( 's_icon_2' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 's_h3_txt_2' ) ); ?></h3>

					<i class="fa fa-caret-up" aria-hidden="true"></i>

					<span></span>

					<div class="service-desc"><?php echo wp_kses_post( get_theme_mod( 's_content_2' ) ); ?></div>

				</div>

			</div>

		</div>

		<div id="service-3" class="col-md-6 col-xl-4 service" data-aos="fade-up" data-aos-delay="400">

			<img class="img-fluid" src="<?php echo esc_url( get_theme_mod( 's_img_url_3' ) ); ?>" alt="<?php echo esc_html( get_theme_mod( 's_h3_txt_3' ) ); ?>">

			<div class="service-details-wrap">

				<div class="service-details">

					<i class="fa <?php echo esc_attr( get_theme_mod( 's_icon_3' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 's_h3_txt_3' ) ); ?></h3>

					<i class="fa fa-caret-up" aria-hidden="true"></i>

					<span></span>

					<div class="service-desc"><?php echo wp_kses_post( get_theme_mod( 's_content_3' ) ); ?></div>

				</div>

			</div>

		</div>

		<div id="service-4" class="col-md-6 col-xl-4 service" data-aos="fade-up">

			<img class="img-fluid" src="<?php echo esc_url( get_theme_mod( 's_img_url_4' ) ); ?>" alt="<?php echo esc_html( get_theme_mod( 's_h3_txt_4' ) ); ?>">

			<div class="service-details-wrap">

				<div class="service-details">

					<i class="fa <?php echo esc_attr( get_theme_mod( 's_icon_4' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 's_h3_txt_4' ) ); ?></h3>

					<i class="fa fa-caret-up" aria-hidden="true"></i>

					<span></span>

					<div class="service-desc"><?php echo wp_kses_post( get_theme_mod( 's_content_4' ) ); ?></div>

				</div>

			</div>

		</div>

		<div id="service-5" class="col-md-6 col-xl-4 service" data-aos="fade-up" data-aos-delay="200">

			<img class="img-fluid" src="<?php echo esc_url( get_theme_mod( 's_img_url_5' ) ); ?>" alt="<?php echo esc_html( get_theme_mod( 's_h3_txt_5' ) ); ?>">

			<div class="service-details-wrap">

				<div class="service-details">

					<i class="fa <?php echo esc_attr( get_theme_mod( 's_icon_5' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 's_h3_txt_5' ) ); ?></h3>

					<i class="fa fa-caret-up" aria-hidden="true"></i>

					<span></span>

					<div class="service-desc"><?php echo wp_kses_post( get_theme_mod( 's_content_5' ) ); ?></div>

				</div>

			</div>

		</div>

		<div id="service-6" class="col-md-6 col-xl-4 service" data-aos="fade-up" data-aos-delay="200">

			<img class="img-fluid" src="<?php echo esc_url( get_theme_mod( 's_img_url_6' ) ); ?>" alt="<?php echo esc_html( get_theme_mod( 's_h3_txt_6' ) ); ?>">

			<div class="service-details-wrap">

				<div class="service-details">

					<i class="fa <?php echo esc_attr( get_theme_mod( 's_icon_6' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 's_h3_txt_6' ) ); ?></h3>

					<i class="fa fa-caret-up" aria-hidden="true"></i>

					<span></span>

					<div class="service-desc"><?php echo wp_kses_post( get_theme_mod( 's_content_6' ) ); ?></div>

				</div>

			</div>

		</div>

	</div>

</div><!-- end our services -->

<div class="blue-divider" data-aos="fade-up"></div>

<!-- about us -->
<div id="about-us" class="container">

	<h1 class="entry-title" data-aos="fade-up"><?php esc_html_e( 'About Us', 'horsesales' ); ?></h1>

	<div class="row">

		<div class="col-xs-12 col-md-6 col-xl-4 offset-xl-2" data-aos="fade-up">

			<p><?php echo esc_html( get_theme_mod( 'about_us_txt' ) ); ?></p>

			<a href="<?php echo esc_url( get_theme_mod( 'more_about_url' ) ); ?>"><i class="fa fa-align-left" aria-hidden="true"></i><?php esc_html_e( 'Read More', 'horsesales' ); ?></a>

		</div>

		<div class="col-xs-12 col-md-6 col-xl-4" data-aos="fade-up" data-aos-delay="200">

			<img src="<?php echo esc_url( get_template_directory_uri() . '/img/logos/logo-green-gradient-transparent.svg' ); ?>" alt="<?php esc_html_e( 'Horsesales.ie: The Future of Irish Horse Sales', 'horsesales' ); ?>">

		</div>

	</div>

</div><!-- end about us -->

<?php get_footer(); ?>
