<?php
/**
 * The template to display the grid of services (i.e. the marketplace).
 *
 * @package Horsesales
 */

get_header();

get_template_part( 'template-parts/breadcrumbs' );

?>

<div id="primary" class="content-area" data-aos="fade-up">

	<main id="main" class="site-main container">

		<div class="row text-center">

			<?php get_template_part( 'template-parts/search-service' ); ?>

		</div>

		<div class="row mt-3">

		<?php

		if ( have_posts() ) :

			while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', 'service-preview' );

				endwhile;

				the_posts_navigation();

				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'horsesales' ),
						'after'  => '</div>',
					)
				);

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</div><!-- .row -->

		<div class="mb-3">

			<a href="<?php echo esc_url( get_theme_mod( 'marketplace_url' ) ); ?>"><i class="fa fa-reply mr-2" aria-hidden="true"></i>Back to Main Marketplace</a>

		</div>

		<?php get_template_part( 'template-parts/cta-service' ); ?>

	</main><!-- #main -->

</div><!-- #primary -->

<?php get_footer(); ?>
