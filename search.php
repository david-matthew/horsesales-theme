<?php
/**
 * The template for displaying search results pages
 *
 * @package Horsesales
 */

get_header();

get_template_part( 'template-parts/breadcrumbs' );

?>

<section id="primary" class="content-area container woocommerce" data-aos="fade-up">

	<main id="main" class="site-main">

	<?php
	if ( have_posts() ) : ?>

		<header class="page-header">

			<h1 class="search-results-h1"><i class="fa fa-list-ul" aria-hidden="true"></i><?php esc_html_e( 'Search Results', 'horsesales' ); ?></h1>

			<p></p>

		</header>

		<div class="row
		<?php
		if ( get_post_type() === 'product' ) {
			echo ' products';
		}
		?>
		">

			<?php
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

	else :

		get_template_part( 'template-parts/content', 'none' );

	endif;
	?>

		</div><!-- .row -->

	</main>

</section><!-- #primary -->

<?php get_footer(); ?>
