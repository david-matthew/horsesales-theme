<?php
/**
 * The template for displaying archive pages
 *
 * @package Horsesales
 */

get_header();

get_template_part( 'template-parts/breadcrumbs' );

?>

<div id="primary" class="content-area container" data-aos="fade-up">

	<main id="main" class="site-main">

	<?php if ( have_posts() ) : ?>

		<header class="page-header">
			<?php
			the_archive_title( '<h1 class="page-title">', '</h1>' );
			the_archive_description( '<div class="archive-description">', '</div>' );
			?>
		</header>

		<?php
		/* Start the Loop */
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content', get_post_type() );

		endwhile;

		the_posts_navigation();

	else :

		get_template_part( 'template-parts/content', 'none' );

	endif;
	?>

	</main><!-- #main -->

</div><!-- #primary -->

<?php get_footer(); ?>
