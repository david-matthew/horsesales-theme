<?php
/**
 * The theme customizer options.
 *
 * @package Horsesales
 */

/**
 * Register the customizer panels and settings.
 *
 * @param object $wp_customize The customizer object.
 */
function horsesales_customize_register( $wp_customize ) {

	/**
	 * Generic settings function, for cleaner customizer code.
	 *
	 * @param object $wp_customize The customizer object.
	 * @param string $name The setting name.
	 * @param string $label The setting label.
	 * @param string $type The setting type.
	 * @param string $section Where to locate the setting.
	 */
	function horsesales_add_setting( $wp_customize, $name, $label, $type, $section ) {
		$wp_customize->add_setting( $name );
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				$name . '_ctl',
				array(
					'label'    => __( $label, 'horsesales' ),
					'type'     => $type,
					'section'  => $section,
					'settings' => $name,
				)
			)
		);
	}

	// Add custom theme options panel.
	$wp_customize->add_panel(
		'horsesales_theme_panel',
		array(
			'capability'  => 'edit_theme_options',
			'title'       => __( 'HorseSales Theme Options', 'horsesales' ),
			'description' => __( 'Customize the Horsesales theme here', 'horsesales' ),
		)
	);

	// Carousel section.
	$wp_customize->add_section(
		'carousel_options',
		array(
			'capability'  => 'edit_theme_options',
			'title'       => __( 'Carousel', 'horsesales' ),
			'description' => __( 'Edit the carousel images, text and links here.', 'horsesales' ),
			'panel'       => 'horsesales_theme_panel',
		)
	);

	// Carousel settings.
	for ( $i = 1; $i <= 3; $i++	) {
		horsesales_add_setting( $wp_customize, 'c_img_url_' . "$i", 'Image URL ' . "$i", 'url', 'carousel_options' );
		horsesales_add_setting( $wp_customize, 'c_main_txt_' . "$i", 'Main Text ' . "$i", 'text', 'carousel_options' );
		horsesales_add_setting( $wp_customize, 'c_btn_txt_' . "$i", 'Button Text ' . "$i", 'text', 'carousel_options' );
		horsesales_add_setting( $wp_customize, 'c_btn_url_' . "$i", 'Button URL ' . "$i", 'url', 'carousel_options' );
	}

	// Services section.
	$wp_customize->add_section(
		'services_options',
		array(
			'capability'  => 'edit_theme_options',
			'title'       => __( 'Services', 'horsesales' ),
			'description' => __( 'Edit the services boxes here.', 'horsesales' ),
			'panel'       => 'horsesales_theme_panel',
		)
	);

	// Services settings.
	horsesales_add_setting( $wp_customize, 's_heading', 'Services Area Heading', 'text', 'services_options' );
	for ( $i = 1; $i <= 6; $i++	) {
		horsesales_add_setting( $wp_customize, 's_img_url_' . "$i", 'Image URL ' . "$i", 'url', 'services_options' );
		horsesales_add_setting( $wp_customize, 's_icon_' . "$i", 'Icon ' . "$i", 'text', 'services_options' );
		horsesales_add_setting( $wp_customize, 's_h3_txt_' . "$i", 'Heading Text ' . "$i", 'text', 'services_options' );
		horsesales_add_setting( $wp_customize, 's_content_' . "$i", 'Content Area ' . "$i", 'textarea', 'services_options' );
	}

	// About Us section.
	$wp_customize->add_section(
		'about_us_options',
		array(
			'capability'  => 'edit_theme_options',
			'title'       => __( 'About Us', 'horsesales' ),
			'description' => __( 'Edit the About Us homepage content here.', 'horsesales' ),
			'panel'       => 'horsesales_theme_panel',
		)
	);

	// About Us settings.
	horsesales_add_setting( $wp_customize, 'about_us_txt', 'The About Us Content', 'textarea', 'about_us_options' );
	horsesales_add_setting( $wp_customize, 'more_about_url', 'The Read More Button URL', 'url', 'about_us_options' );

	// Ads Pro section.
	$wp_customize->add_section(
		'ads_pro_options',
		array(
			'capability'  => 'edit_theme_options',
			'title'       => __( 'Ads Pro Shortcodes', 'horsesales' ),
			'description' => __( 'Place your Ads Pro shortcodes here.', 'horsesales' ),
			'panel'       => 'horsesales_theme_panel',
		)
	);

	// Ads Pro settings.
	horsesales_add_setting( $wp_customize, 'ads_pro_home_desktop', 'Homepage Ad Shortcode (Desktop)', 'text', 'ads_pro_options' );
	horsesales_add_setting( $wp_customize, 'ads_pro_home_tablet', 'Homepage Ad Shortcode (Tablet)', 'text', 'ads_pro_options' );
	horsesales_add_setting( $wp_customize, 'ads_pro_home_mobile', 'Homepage Ad Shortcode (Mobile)', 'text', 'ads_pro_options' );
	horsesales_add_setting( $wp_customize, 'ads_pro_products_desktop', 'Products/Horses Ad Shortcode (Desktop)', 'text', 'ads_pro_options' );
	horsesales_add_setting( $wp_customize, 'ads_pro_products_tablet', 'Products/Horses Ad Shortcode (Tablet)', 'text', 'ads_pro_options' );
	horsesales_add_setting( $wp_customize, 'ads_pro_products_mobile', 'Products/Horses Ad Shortcode (Mobile)', 'text', 'ads_pro_options' );

	// Marketplace section.
	$wp_customize->add_section(
		'marketplace_options',
		array(
			'capability'  => 'edit_theme_options',
			'title'       => __( 'Marketplace', 'horsesales' ),
			'description' => __( 'Set the Marketplace URL and category images here.', 'horsesales' ),
			'panel'       => 'horsesales_theme_panel',
		)
	);

	// Marketplace settings.
	horsesales_add_setting( $wp_customize, 'marketplace_url', 'Marketplace URL', 'url', 'marketplace_options' );
	horsesales_add_setting( $wp_customize, 'mkt_submit_url', 'Marketplace Submission URL', 'url', 'marketplace_options' );
	for ( $i = 1; $i <= 14; $i++ ) {
		horsesales_add_setting( $wp_customize, 'plc_title_' . "$i", 'Placeholder Title ' . "$i", 'text', 'marketplace_options' );
		horsesales_add_setting( $wp_customize, 'plc_img_url_' . "$i", 'Placeholder Image URL ' . "$i", 'url', 'marketplace_options' );
		horsesales_add_setting( $wp_customize, 'plc_cat_url_' . "$i", 'Placeholder Category URL ' . "$i", 'url', 'marketplace_options' );
	}

	// Footer section.
	$wp_customize->add_section(
		'footer_options',
		array(
			'capability'  => 'edit_theme_options',
			'title'       => __( 'Footer', 'horsesales' ),
			'description' => __( 'Set the footer links here', 'horsesales' ),
			'panel'       => 'horsesales_theme_panel',
		)
	);

	// Footer settings.
	for ( $i = 1; $i <= 4; $i++	) {
		horsesales_add_setting( $wp_customize, 'ft_txt_' . "$i", 'Footer Text ' . "$i", 'text', 'footer_options' );
		horsesales_add_setting( $wp_customize, 'ft_url_' . "$i", 'Footer Link ' . "$i", 'url', 'footer_options' );
	}
}
add_action( 'customize_register', 'horsesales_customize_register' );
