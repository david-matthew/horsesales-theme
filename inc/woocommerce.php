<?php
/**
 * WooCommerce Customisations
 *
 * As of v3.3, Woocommerce appears to work better with 3rd party themes without formally declaring support.
 *
 * @package Horsesales
 */

/**
 * Prevent display of related products.
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/**
 * Prevent display of SKU.
 */
add_filter( 'wc_product_sku_enabled', '__return_false' );

/**
 * Conditionally set the product sorting dropdowns.
 *
 * @param array $orderby The array of product sorting options.
 */
function horsesales_catalog_orderby( $orderby ) {
	unset( $orderby['popularity'] );
	unset( $orderby['rating'] );

	if ( is_product_category( 'Thoroughbred Auctions' ) ) {
		unset( $orderby['price'] );
		unset( $orderby['price-desc'] );
	} else {
		unset( $orderby['bid_asc'] );
		unset( $orderby['bid_desc'] );
		unset( $orderby['auction_end'] );
		unset( $orderby['auction_started'] );
		unset( $orderby['auction_activity'] );
	}

	return $orderby;
}
// add_filter( 'woocommerce_catalog_orderby', 'horsesales_catalog_orderby', 20 );

/**
 * Hide the product sorting dropdowns completely.
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * Remove unnecessary product tabs.
 *
 * @param array $tabs The array of product tabs.
 */
function horsesales_remove_product_tabs( $tabs ) {
	unset( $tabs['description'] );
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'horsesales_remove_product_tabs', 98 );

/**
 * Remove the main editor for products.
 */
function horsesales_remove_product_editor() {
	remove_post_type_support( 'product', 'editor' );
}
add_action( 'init', 'horsesales_remove_product_editor' );

/**
 * Translate products to horses and tags to price-bracket.
 *
 * @param string $translated The text to translate.
 */
function horsesales_translate_text( $translated ) {
	// $translated = str_ireplace( 'Product', 'Horse', $translated );
	$translated = str_ireplace( 'short description', 'description', $translated );
	if ( is_product() ) {
		$translated = str_ireplace( 'tag', 'Price Bracket', $translated );
	}
	return $translated;
}
add_filter( 'gettext', 'horsesales_translate_text' );
add_filter( 'ngettext', 'horsesales_translate_text' );

if ( ! function_exists( 'horsesales_woocommerce_product_columns_wrapper_close' ) ) {
	/**
	 * Product columns wrapper close.
	 *
	 * @return  void
	 */
	function horsesales_woocommerce_product_columns_wrapper_close() {
		echo '</div>';
	}
}
add_action( 'woocommerce_after_shop_loop', 'horsesales_woocommerce_product_columns_wrapper_close', 40 );

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

/**
 * Show the sold status in the product loop for auctions.
 */
function horsesales_sold_status() {
	global $product;

	if ( ! isset( $product ) ) {
		return;
	}

	if ( has_term( 'sold', 'product_cat' ) ) {
		echo '<span class="sold-status">SOLD</span>';
	}
}
add_action( 'woocommerce_after_shop_loop_item_title', 'horsesales_sold_status', 48 );

/**
 * Unhide the author field.
 */
function hs_unhide_author() {
	add_post_type_support( 'product', 'author' );
}
add_action( 'init', 'hs_unhide_author', 999 );
