<?php
/**
 * The file containing the breadcrumb function.
 *
 * @package Horsesales
 */

/**
 * Breadcrumb function.
 */
function hs_breadcrumb() {

	global $wp_query;
	$queried   = $wp_query->get_queried_object();
	$separator = '&nbsp;&#47;&nbsp;';

	echo '<a href="' . esc_url( home_url() ) . '" rel="nofollow">Home</a>';
	echo esc_attr( $separator );

	$parents = get_post_ancestors( $queried->ID );
	if ( $parents ) {
		foreach ( $parents as $p ) {
			echo esc_html( $p ) . esc_attr( $separator );
		}
	}

	the_title();
}
