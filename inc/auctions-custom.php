<?php
/**
 * WooCommerce Simple Auctions Customisation
 *
 * @link http://wpgenie.org/woocommerce-simple-auctions/documentation/
 *
 * @package Horsesales
 */

/**
 * Restrict bidding based on role
 */
function horsesales_check_user_role() {
	$current_user = wp_get_current_user();
	if ( in_array( 'subscriber', $current_user->roles, true ) ) {
		$notice_url = esc_url( get_theme_mod( 'register_to_bid_url' ) );
		$notice     = 'You must be an approved bidder to bid on this site. Click <a href="' . $notice_url . '">here</a> to register to bid.';
		wc_add_notice( $notice, 'error' );
		return false;
	} else {
		// User can place a bid.
		return true;
	}
}
add_filter( 'woocommerce_simple_auctions_before_place_bid_filter', 'horsesales_check_user_role' );

/**
 * Show the sold status in the product loop for auctions.
 */
function horsesales_auction_status() {
	global $product;

	if ( ! isset( $product ) || $product->get_type() !== 'auction' ) {
		return;
	}

	if ( $product->is_finished() && $product->is_reserve_met() ) {
		echo '<span class="sold-status">SOLD</span>';
	}

}
add_action( 'woocommerce_after_shop_loop_item_title', 'horsesales_auction_status', 48 );

/**
 * Show the excerpt in the product loop
 */
function horsesales_excerpt() {
	global $product;

	if ( ! isset( $product ) ) {
		return;
	}

	$excerpt = wp_trim_words( $product->get_short_description(), 20, '<span class="read-more">...read more</span>' );
	echo '<p class="excerpt">' . $excerpt . '</p>';

}
add_action( 'woocommerce_after_shop_loop_item_title', 'horsesales_excerpt', 49 );

/**
 * Show the counter in the product loop
 */
function horsesales_counter() {
	global $product;

	if ( ! isset( $product ) || $product->get_type() !== 'auction' ) {
		return;
	}

	$time = '';

	$timetext = esc_html__( 'Time left:', 'horsesales' );

	if ( ! $product->is_started() ) {
		$timetext     = esc_html__( 'Starting in:', 'horsesales' );
		$counter_time = $product->get_seconds_to_auction();
	} else {
		$counter_time = $product->get_seconds_remaining();
	}

	$future_class = ( $product->is_closed() === false ) && ( $product->is_started() === false ) ? 'future' : '';

	$time = '<span class="time-left ">' . $timetext . '</span>
	<div class="auction-time-countdown ' . $future_class . '"
	data-time="' . esc_attr( $counter_time ) . '"
	data-auctionid="' . intval( $product->get_id() ) . '" data-format="' . esc_attr( get_option( 'simple_auctions_countdown_format' ) ) . '"></div>';

	if ( $product->is_closed() ) {
		$time = '';
	}

	echo $time;
}
add_action( 'woocommerce_after_shop_loop_item_title', 'horsesales_counter', 50 );

/**
 * Embed bid and increment values in data attributes for use in front-end.
 */
function horsesales_increment_info() {
	global $product;
	$current_bid = $product->get_curent_bid();
	$increment   = $product->get_increase_bid_value();
	echo '<p>Minimum increment between bids: €<span data-current-bid="' . esc_html( $current_bid ) . '" data-increment="' . esc_html( $increment ) . '" class="increment-value">' . esc_html( $increment ) . '</span></p>';
}
add_action( 'woocommerce_before_bid_form', 'horsesales_increment_info', 50 );

/**
 * Add the read more section to the horse/product page.
 */
function horsesales_read_more() {
	global $post;
	if ( ! empty( $post->read_more_txt ) ) {
		?>
		<div id="more-text" class="collapse">
			<?php echo wp_kses_post( $post->read_more_txt ); ?>
		</div>
		<button id="more-text-btn" data-toggle="collapse" data-target="#more-text" aria-expanded="false" aria-controls="more-text">
			<i class="fa fa-align-left" aria-hidden="true"></i>Read More
		</button>
		<?php
	}
}
add_action( 'woocommerce_single_product_summary', 'horsesales_read_more', 24 );

/**
 * Add a video to the horse/product page.
 */
function horsesales_video() {
	global $post;
	if ( ! empty( $post->horse_video ) ) {
		?>
		<!-- <div class="video-link-wrapper"> -->
			<a class="video-link" data-lity href="<?php echo esc_url( $post->horse_video ); ?>">
				<i class="fa fa-video-camera" aria-hidden="true"></i><?php echo esc_html( $post->horse_video_txt ); ?>
			</a>
			<br>
		<!-- </div> -->
		<?php
	}
}
add_action( 'woocommerce_single_product_summary', 'horsesales_video', 25 );

/**
 * Add share and request to view buttons to the horse/product page.
 */
function horsesales_view_share() {
	// Show request viewing button if product type is an auction.
	global $product;
	if ( $product->get_type() === 'auction' ) {
		$request_viewing_link = get_theme_mod( 'request_viewing_url' );
		?>
		<p class="wsawl-link">
			<a class="request-viewing" target="_blank" href="<?php echo esc_url( $request_viewing_link ); ?>">Request a Viewing</a>
		</p>
	<?php } ?>
	<p class="wsawl-link">
		<a class="share-horse" role="button">Share</a>
	</p>
	<input type="text" id="shareable-link" name="shareable-link" value="<?php echo esc_url( get_permalink() ); ?>">
	<?php
}
add_action( 'woocommerce_single_product_summary', 'horsesales_view_share', 26 );
