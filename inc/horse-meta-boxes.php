<?php
/**
 * Horse Meta Boxes.
 *
 * Adding custom meta boxes to the horse (product) post type.
 *
 * @package Horsesales
 */

/**
 * Add the metabox.
 */
function hs_add_horse_metabox() {
	add_meta_box(
		'horse_fields',
		'Horse Custom Fields',
		'hs_horse_metabox_callback',
		'product',
		'normal',
		'default'
	);
}

/**
 * The metabox callback function.
 *
 * @param object $post The post object.
 */
function hs_horse_metabox_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'horse_fields_nonce' );
	$hs_stored_meta = get_post_meta( $post->ID );
	?>
	<div class="wrap horsesales-fields">

		<div class="form-group">

			<label for="read-more-txt"><?php esc_html_e( 'Read More Section', 'horsesales' ); ?></label>

			<textarea rows="5" name="read_more_txt" id="read-more-txt"><?php if ( isset( $hs_stored_meta['read_more_txt'] ) ) echo wp_kses_post( $hs_stored_meta['read_more_txt'][0] ); ?></textarea>

		</div>

	</div>
	<?php
}
add_action( 'add_meta_boxes', 'hs_add_horse_metabox' );

/**
 * The metabox save function.
 *
 * @param string $post_id The post ID.
 */
function hs_meta_save( $post_id ) {
	$is_autosave    = wp_is_post_autosave( $post_id );
	$is_revision    = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST['horse_fields_nonce'] ) && wp_verify_nonce( $_POST['horse_fields_nonce'], basename( __FILE__ ) ) ) ? 'true' : 'false';

	if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
		return;
	}

	if ( isset( $_POST['read_more_txt'] ) ) {
		update_post_meta( $post_id, 'read_more_txt', wp_kses_post( wp_unslash( $_POST['read_more_txt'] ) ) );
	}
}
add_action( 'save_post', 'hs_meta_save' );
