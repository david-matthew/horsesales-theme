<?php
/**
 * The template part for displaying service post type preview items/cards.
 *
 * @package Horsesales
 */

// Fetch the data we need.
$service_id    = get_the_ID();
$service_title = get_the_title();
$website       = get_post_meta( $service_id, 'service_website', true );
$facebook      = get_post_meta( $service_id, 'service_facebook', true );
$contact       = get_post_meta( $service_id, 'service_phn', true );
$sc_terms      = get_the_terms( $service_id, 'service-category' );
$service_cat   = ( ! empty( $sc_terms ) ) ? join( ', ', wp_list_pluck( $sc_terms, 'name' ) ) : '';
$loc_terms     = get_the_terms( $service_id, 'service-location' );
$location      = ( ! empty( $loc_terms ) ) ? join( ', ', wp_list_pluck( $loc_terms, 'name' ) ) : '';
?>

<div class="col-xs-12 col-md-8 offset-md-2 col-lg-6 offset-lg-0">

	<div class="service-preview">

		<div class="left-wrap">

			<img src="<?php the_post_thumbnail_url( 'medium_large' ); ?>" alt="<?php the_title(); ?>">

			<div class="left-content">

				<div class="service-heading-wrap">

					<div class="service-heading center-absolute">

						<i class="fa fa-building" aria-hidden="true"></i>

						<h3>
							<?php
							if ( strlen( $service_title ) > 21 ) {
								$service_title = substr( $service_title, 0, 18 ) . '...';
								echo esc_html( $service_title );
							} else {
								echo esc_html( $service_title );
							}
							?>
						</h3>

						<i class="fa fa-caret-up" aria-hidden="true"></i>

					</div>

				</div>

				<?php

				$custom_excerpt = substr( get_the_content(), 0, 100 );

				echo '<p>' . esc_html( $custom_excerpt ) . '...</p>';

				?>

				<div class="service-link-wrap">

					<div class="service-link center-absolute">

						<a href="<?php the_permalink(); ?>"><i class="fa fa-align-left" aria-hidden="true"></i>Read More</a>

					</div>

				</div>

			</div>

		</div>

		<div class="right-wrap">

			<div class="service-field-row">

				<div class="service-field">

					<?php
					// Echoing icon to prevent automatic addition of whitespace.
					echo '<i class="fa fa-tag" aria-hidden="true"></i>';

					if ( strlen( $service_cat ) > 13 ) {
						$service_cat = substr( $service_cat, 0, 13 ) . '...';
						echo esc_html( $service_cat );
					} else {
						echo esc_html( $service_cat );
					}
					?>

				</div>

			</div>

			<div class="service-field-row">

				<div class="service-field">

					<?php
					echo '<i class="fa fa-map-marker" aria-hidden="true"></i>';

					if ( strlen( $location ) > 13 ) {
						$location = substr( $location, 0, 13 ) . '...';
						echo esc_html( $location );
					} else {
						echo esc_html( $location );
					}
					?>

				</div>

			</div>

			<div class="service-field-row">

				<div class="service-field">

					<?php

					if ( ! empty( $website ) ) {
						echo '<i class="fa fa-link" aria-hidden="true"></i><a href="' . esc_url( $website ) . '" target="_blank">';
						esc_html_e( 'Website', 'horsesales' );
						echo '<i class="fa fa-external-link" aria-hidden="true"></i></a>';
					} else {
						echo '<i class="fa fa-align-left" aria-hidden="true"></i><a href="';
						the_permalink();
						echo '">';
						esc_html_e( 'Read More', 'horsesales' );
						echo '</a>';
					}

					?>

				</div>

			</div>

			<div class="service-field-row">

				<div class="service-field">

					<i class="fa fa-phone" aria-hidden="true"></i><span class="show-on-hover"><?php echo esc_html( $contact ); ?></span><span class="hide-on-hover">Show Number</span>

				</div>

			</div>

		</div>

	</div>

</div>
