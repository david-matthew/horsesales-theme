<?php
/**
 * Template part for displaying page content in page.php
 *
 * @package Horsesales
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">

		<?php

		global $page_banner_img_url;

		if ( ! $page_banner_img_url ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
		}

		if ( is_product_category() ) {
			?>
			<!-- ad space -->
			<div class="container">
				<?php
				if ( get_theme_mod( 'ads_pro_products_desktop' ) ) {
					echo do_shortcode( get_theme_mod( 'ads_pro_products_desktop' ) );
				}

				if ( get_theme_mod( 'ads_pro_products_tablet' ) ) {
					echo do_shortcode( get_theme_mod( 'ads_pro_products_tablet' ) );
				}

				if ( get_theme_mod( 'ads_pro_products_mobile' ) ) {
					echo do_shortcode( get_theme_mod( 'ads_pro_products_mobile' ) );
				}
				?>
			</div>
			<?php
			get_template_part( 'template-parts/search-product' );
		}
		?>

	</header>

	<div class="entry-content">

		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'horsesales' ),
				'after'  => '</div>',
			)
		);
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
