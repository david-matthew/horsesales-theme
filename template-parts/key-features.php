<?php
/**
 * The template part for displaying the key features block.
 *
 * @package Horsesales
 */

?>

<div id="key-features">

	<div class="container">

		<div class="row text-center">

			<h1 class="entry-title" data-aos="fade-up">Key Features</h1>

		</div>

		<div class="row">

			<div class="col-xs-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="200">

				<div id="key-feature-1" class="key-feature text-center">

					<i class="fa <?php echo esc_attr( get_theme_mod( 'kf_i_1' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 'kf_h_1' ) ); ?></h3>

					<div class="underline"></div>

					<p><?php echo esc_html( get_theme_mod( 'kf_txt_1' ) ); ?></p>

				</div>

			</div>

			<div class="col-xs-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="400">

				<div id="key-feature-2" class="key-feature text-center">

					<i class="fa <?php echo esc_attr( get_theme_mod( 'kf_i_2' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 'kf_h_2' ) ); ?></h3>

					<div class="underline"></div>

					<p><?php echo esc_html( get_theme_mod( 'kf_txt_2' ) ); ?></p>

				</div>

			</div>

			<div class="col-xs-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="600">

				<div id="key-feature-3" class="key-feature text-center">

					<i class="fa <?php echo esc_attr( get_theme_mod( 'kf_i_3' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 'kf_h_3' ) ); ?></h3>

					<div class="underline"></div>

					<p><?php echo esc_html( get_theme_mod( 'kf_txt_3' ) ); ?></p>

				</div>

			</div>

			<div class="col-xs-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="800">

				<div id="key-feature-4" class="key-feature text-center">

					<i class="fa <?php echo esc_attr( get_theme_mod( 'kf_i_4' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 'kf_h_4' ) ); ?></h3>

					<div class="underline"></div>

					<p><?php echo esc_html( get_theme_mod( 'kf_txt_4' ) ); ?></p>

				</div>

			</div>

		</div><!-- /.row -->

		<div class="row">

			<div class="col-xs-12 col-md-6 col-lg-3 custom-lg-offset" data-aos="fade-up">

				<div id="key-feature-5" class="key-feature text-center">

					<i class="fa <?php echo esc_attr( get_theme_mod( 'kf_i_5' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 'kf_h_5' ) ); ?></h3>

					<div class="underline"></div>

					<p><?php echo esc_html( get_theme_mod( 'kf_txt_5' ) ); ?></p>

				</div>

			</div>

			<div class="col-xs-12 col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="200">

				<div id="key-feature-6" class="key-feature text-center">

					<i class="fa <?php echo esc_attr( get_theme_mod( 'kf_i_6' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 'kf_h_6' ) ); ?></h3>

					<div class="underline"></div>

					<p><?php echo esc_html( get_theme_mod( 'kf_txt_6' ) ); ?></p>

				</div>

			</div>

			<div class="col-xs-12 col-md-6 offset-md-3 col-lg-3 offset-lg-0" data-aos="fade-up" data-aos-delay="400">

				<div id="key-feature-7" class="key-feature text-center">

					<i class="fa <?php echo esc_attr( get_theme_mod( 'kf_i_7' ) ); ?>" aria-hidden="true"></i>

					<h3><?php echo esc_html( get_theme_mod( 'kf_h_7' ) ); ?></h3>

					<div class="underline"></div>

					<p><?php echo esc_html( get_theme_mod( 'kf_txt_7' ) ); ?></p>

				</div>

			</div>

		</div>

	</div>

</div><!-- / #key-features -->
