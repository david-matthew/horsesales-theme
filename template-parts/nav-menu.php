<?php
/**
 * The template part for displaying top navigation menu.
 *
 * @package Horsesales
 */

$logo        = get_template_directory_uri() . '/img/logos/logo-with-text-green.svg';
$logo_mobile = get_template_directory_uri() . '/img/logos/logo-green.svg';

?>

<nav id="navbar" class="navbar fixed-top navbar-expand-xl" role="navigation">

	<div class="container"

	<?php

	if ( is_home() ) {
		echo ' data-aos="fade-down"';
	}

	?>
	>

		<a id="logo" class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>">

			<img src="<?php echo esc_url( $logo ); ?>" alt="Horsesales Logo">

		</a>

		<i id="mobile-search" class="search-btn fa fa-search" aria-hidden="true"></i>

		<a id="logo-mobile" class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>">

			<img src="<?php echo esc_url( $logo_mobile ); ?>" alt="Horsesales Logo">

		</a>

		<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

			<span class="icon-bar top-bar"></span>

			<span class="icon-bar middle-bar"></span>

			<span class="icon-bar bottom-bar"></span>

		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<ul class="navbar-nav ml-auto">

				<li id="desktop-search" class="search-btn fa fa-search" aria-hidden="true"><span>Search</span></li>

				<?php

				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
						'menu_class'     => 'nav navbar-nav',
					)
				);

				?>

			</ul>

		</div>

	</div><!-- /.container -->

</nav>
