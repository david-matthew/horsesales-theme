<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @package Horsesales
 */

?>

<section class="no-results not-found">

	<header class="page-header">

		<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'horsesales' ); ?></h1>

	</header>

	<div class="page-content">

		<?php if ( is_search() ) : ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms.', 'horsesales' ); ?></p>

		<?php else : ?>

			<p><?php esc_html_e( 'Sorry, but it seems we can&rsquo;t find what you&rsquo;re looking for.', 'horsesales' ); ?></p>

		<?php endif; ?>

	</div><!-- .page-content -->

</section><!-- .no-results -->
