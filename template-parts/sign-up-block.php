<?php
/**
 * The template part for displaying the sign up block.
 *
 * @package Horsesales
 */

?>

<div id="sign-up-block">

	<div class="bg-img">

		<div class="overlay"></div>

		<div class="container center-absolute">

			<div class="row">

				<div class="offset-lg-3 col-lg-3" data-aos="fade-up">

					<a href="<?php echo esc_url( get_theme_mod( 'sign_up_url' ) ); ?>">

						<i id="sign-up-icon" class="fa fa-user-plus"></i>

					</a>

				</div>

				<div class="col-xs-12 col-lg-3" data-aos="fade-up">

					<p>Not already a member? Sign up for Horsesales.ie today - it's free to join and you can view all our live auctions straight away!</p>

					<a href="<?php echo esc_url( get_theme_mod( 'sign_up_url' ) ); ?>">

						<div class="home-page-btn">

							<span>Sign Up</span>

							<i class="fa fa-user-plus" aria-hidden="true"></i>

						</div>

					</a>

				</div>

			</div>

		</div>

	</div>

</div>
