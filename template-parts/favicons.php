<?php
/**
 * The template for displaying cross-platform favicons
 *
 * @package Horsesales
 */

$favicons_path = get_template_directory_uri() . '/img/favicons';
?>

<link rel="icon" type="image/png" href="<?php echo esc_url( $favicons_path . '/favicon.png' ); ?>">
<link rel="apple-touch-icon" href="<?php echo esc_url( $favicons_path . '/touch-icon-iphone.png' ); ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo esc_url( $favicons_path . '/touch-icon-ipad.png' ); ?>">
<link rel="apple-touch-icon" sizes="167x167" href="<?php echo esc_url( $favicons_path . '/touch-icon-ipad-retina.png' ); ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url( $favicons_path . '/touch-icon-iphone-retina.png' ); ?>">
<link rel="icon" sizes="192x192" href="<?php echo esc_url( $favicons_path . '/android-icon.png' ); ?>">
<meta name="msapplication-square310x310logo" content="<?php echo esc_url( $favicons_path . '/microsoft-icon.png' ); ?>">
