<?php
/**
 * Template part for displaying the service search/filter form
 *
 * @package Horsesales
 */

?>

<div class="mkt-search-container col-12">

	<form role="search" method="get" action="<?php echo esc_url( site_url() ); ?>">

		<div class="form-row">

			<div class="form-group input-group col-12 col-md-5 col-lg-4 col-xl-3">

				<div class="input-group-prepend">

					<span class="input-group-text"><i class="fa fa-list-ul" aria-hidden="true"></i></span>

				</div>

				<select name="service-category" id="service-category" class="form-control">
					<option value=""><?php esc_html_e( 'All Categories', 'horsesales' ); ?></option>
					<?php

					// Retrieve the service categories.
					$service_categories = get_categories(
						array(
							'taxonomy'   => 'service-category',
							'orderby'    => 'name',
							'show_count' => 1,
							'pad_counts' => 0,
							'title_li'   => '',
							'hide_empty' => false,
						)
					);

					// Loop through them and display as option elements.
					foreach ( $service_categories as $sc ) {
						echo '<option value="' . esc_html( $sc->slug ) . '">' . esc_html( $sc->name ) . '</option>';
					}

					?>
				</select>

			</div>

			<div class="form-group input-group col-8 col-md-4 col-xl-3">

				<div class="input-group-prepend">

					<span class="input-group-text"><i class="fa fa-map-marker" aria-hidden="true"></i></span>

				</div>

				<select name="service-location" id="service-location" class="form-control">
					<option value=""><?php esc_html_e( 'All Locations', 'horsesales' ); ?></option>
					<?php

					// Retrieve the service locations.
					$service_locations = get_categories(
						array(
							'taxonomy'   => 'service-location',
							'orderby'    => 'name',
							'show_count' => 1,
							'pad_counts' => 0,
							'title_li'   => '',
							'hide_empty' => false,
						)
					);

					// Loop through them and display as option elements.
					foreach ( $service_locations as $loc ) {
						echo '<option value="' . esc_html( $loc->slug ) . '">' . esc_html( $loc->name ) . '</option>';
					}
					?>
				</select>

			</div>

			<div class="form-group input-group col-4 col-md-3 col-lg-2">

				<input type="hidden" name="post_type" value="service">

				<button type="submit" class="btn btn-primary"><i class="fa fa-search mr-1" aria-hidden="true"></i><?php esc_html_e( 'Search', 'horsesales' ); ?></button>

			</div>

		</div>

	</form>

</div>