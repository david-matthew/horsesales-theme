<?php
/**
 * The template part for displaying the homepage banner (replaced in May 2019).
 *
 * @package Horsesales
 */

?>

<div class="banner-wrap" data-aos="fade-in">

	<div class="bg-img home-page-banner">

		<div class="overlay"></div>

	</div>

</div>

<div class="container banner-content center-absolute">

	<div data-aos="fade-up">

		<div class="row">

			<div class="col-xs-12 mx-auto">

				<img id="text-tagline" src="<?php echo esc_url( $text_tagline ); ?>" alt="The future of throughbred and sport horse sales">

			</div>

		</div>

		<div class="row">

			<div class="col-xl-8" >

				<img id="logo-text-tagline" src="<?php echo esc_url( $logo_text_tagline ); ?>" alt="The future of throughbred and sport horse sales">

			</div>

			<div class="col-lg-12 col-xl-4">

				<a href="<?php echo esc_url( get_theme_mod( 'btn_url_1' ) ); ?>">

					<div id="banner-btn-1" class="home-page-btn mx-auto">

						<span><?php echo esc_html( get_theme_mod( 'btn_txt_1' ) ); ?></span>

						<i class="fa <?php echo esc_attr( get_theme_mod( 'btn_icon_1' ) ); ?>"></i>

					</div> 

				</a>

				<a href="<?php echo esc_url( get_theme_mod( 'btn_url_2' ) ); ?>">

					<div id="banner-btn-2" class="home-page-btn mx-auto">

						<span><?php echo esc_html( get_theme_mod( 'btn_txt_2' ) ); ?></span>

						<i class="fa <?php echo esc_attr( get_theme_mod( 'btn_icon_2' ) ); ?>"></i>

					</div> 

				</a>

				<a href="<?php echo esc_url( get_theme_mod( 'btn_url_3' ) ); ?>">

					<div id="banner-btn-3" class="home-page-btn mx-auto">

						<span><?php echo esc_html( get_theme_mod( 'btn_txt_3' ) ); ?></span>

						<i class="fa <?php echo esc_attr( get_theme_mod( 'btn_icon_3' ) ); ?>"></i>

					</div> 

				</a>

				<a href="<?php echo esc_url( get_theme_mod( 'btn_url_4' ) ); ?>">

					<div id="banner-btn-4" class="home-page-btn mx-auto">

						<span><?php echo esc_html( get_theme_mod( 'btn_txt_4' ) ); ?></span>

						<i class="fa <?php echo esc_attr( get_theme_mod( 'btn_icon_4' ) ); ?>"></i>

					</div> 

				</a>

			</div>

		</div>

	</div>

</div><!-- end .banner-content -->
