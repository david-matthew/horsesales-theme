<?php
/**
 * The template part for displaying service post types.
 *
 * @package Horsesales
 */

// Fetch the data we need.
$service_id    = get_the_ID();
$featured_img  = get_the_post_thumbnail_url();
$add_img_1_url = get_post_meta( $service_id, 'service_additional_img_1', true );
$add_img_2_url = get_post_meta( $service_id, 'service_additional_img_2', true );
$add_img_3_url = get_post_meta( $service_id, 'service_additional_img_3', true );
$video_id      = get_post_meta( $service_id, 'service_video_id', true );
$logo_url      = get_post_meta( $service_id, 'service_logo_url', true );
$loc_terms     = get_the_terms( $service_id, 'service-location' );
$sc_terms      = get_the_terms( $service_id, 'service-category' );
$location      = ( ! empty( $loc_terms ) ) ? join( ', ', wp_list_pluck( $loc_terms, 'name' ) ) : '';
$service_cat   = ( ! empty( $sc_terms ) ) ? join( ', ', wp_list_pluck( $sc_terms, 'name' ) ) : '';
$email         = get_post_meta( $service_id, 'service_email', true );
$tel_no        = get_post_meta( $service_id, 'service_phn', true );
$website       = get_post_meta( $service_id, 'service_website', true );
$facebook      = get_post_meta( $service_id, 'service_facebook', true );
$instagram     = get_post_meta( $service_id, 'service_instagram', true );
$linkedin      = get_post_meta( $service_id, 'service_linkedin', true );
$twitter       = get_post_meta( $service_id, 'service_twitter', true );
$google_maps   = get_post_meta( $service_id, 'service_gmaps', true );
$address       = get_post_meta( $service_id, 'service_address', true );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div id="service-header" class="row">

		<div class="col-12">

			<div class="service-logo-wrap border-grey">

				<?php

				if ( ! empty( $logo_url ) ) {
					?>
					<img src="<?php echo esc_url( $logo_url ); ?>" alt="<?php the_title(); ?>">
					<?php
				} else {
					?>
					<img src="<?php echo esc_url( get_template_directory_uri() . '/img/service-logo-placeholder.svg' ); ?>" alt="<?php the_title(); ?>">
					<?php
				}
				?>

			</div>

			<div class="service-title-wrap">

				<h1><?php the_title(); ?></h1>

				<span class="service-location"><i class="fa fa-map-marker" aria-hidden="true"></i>

				<?php

				if ( ! empty( $address ) ) {
					echo esc_html( $address );
				} else {
					echo esc_html( $location );
				}

				?>

				</span>

			</div>

		</div>

	</div>

	<div class="row">	

		<div class="col-12 col-lg-7 col-xl-8">

			<div id="service-carousel" class="border-grey carousel slide carousel-fade" data-ride="carousel">

				<div class="carousel-inner">

					<div class="carousel-item active">

						<?php

						$media_count = 0;

						if ( ! empty( $featured_img ) ) {
							$media_count++;
							?>
							<img src="<?php echo esc_url( $featured_img ); ?>" class="d-block w-100" alt="<?php the_title(); ?>">
							<?php
						}

						?>

					</div>

					<?php

					if ( ! empty( $video_id ) ) {
						$media_count++;
						?>

						<div class="carousel-item">

							<img src="https://img.youtube.com/vi/<?php echo esc_html( $video_id ); ?>/0.jpg" class="d-block w-100" alt="<?php the_title(); ?>-video">

							<div class="service-video">

								<a href="https://www.youtube.com/watch?v=<?php echo esc_html( $video_id ); ?>" data-lity><i class="fa fa-youtube-play" aria-hidden="true"></i></a>

							</div>

						</div>

						<?php
					}

					if ( ! empty( $add_img_1_url ) ) {
						$media_count++;
						?>

						<div class="carousel-item">

							<img src="<?php echo esc_url( $add_img_1_url ); ?>" class="d-block w-100" alt="<?php the_title(); ?>-2">

						</div>

						<?php
					}

					if ( ! empty( $add_img_2_url ) ) {
						$media_count++;
						?>

						<div class="carousel-item">

							<img src="<?php echo esc_url( $add_img_2_url ); ?>" class="d-block w-100" alt="<?php the_title(); ?>-3">

						</div>

						<?php
					}

					if ( ! empty( $add_img_3_url ) ) {
						$media_count++;
						?>

						<div class="carousel-item">

							<img src="<?php echo esc_url( $add_img_3_url ); ?>" class="d-block w-100" alt="<?php the_title(); ?>-4">

						</div>

						<?php
					}

					?>

				</div><!-- /.carousel-inner -->

				<?php

				if ( $media_count > 1 ) :

					?>

					<a class="carousel-control-prev" href="#service-carousel" role="button" data-slide="prev">

						<i class="fa fa-chevron-left" aria-hidden="true"></i>

						<span class="sr-only"><?php esc_html_e( 'Previous', 'horsesales' ); ?></span>

					</a>

					<a class="carousel-control-next" href="#service-carousel" role="button" data-slide="next">

						<i class="fa fa-chevron-right" aria-hidden="true"></i>

						<span class="sr-only"><?php esc_html_e( 'Next', 'horsesales' ); ?></span>

					</a>

					<?php

				endif;

				?>

			</div><!-- /#service-carousel -->

		</div>

		<div class="col-12 col-lg-5 col-xl-4">

			<div id="service-contact-box" class="row border-grey">

				<div class="col-12 service-contact-wrap">

					<div class="button-wrap">

						<button>

							<a href="mailto:<?php echo esc_html( $email ); ?>?Subject=Contact%20via%20HorseSales.ie&cc=service@horsesales.ie">

								<i class="fa fa-envelope-o" aria-hidden="true"></i><?php esc_html_e( 'Send Email', 'horsesales' ); ?>

							</a>

						</button>

					</div>

					<div class="button-wrap">

						<button>

							<a href="tel:<?php echo esc_html( $tel_no ); ?>">

								<i class="fa fa-phone" aria-hidden="true"></i><?php echo esc_html( $tel_no ); ?>

							</a>

						</button>

					</div>

				</div>

			</div><!-- /#service-contact-box -->

			<?php
			if ( ! empty( $website ) || ! empty( $facebook ) || ! empty( $instagram ) || ! empty( $twitter ) || ! empty( $linkedin ) || ! empty( $google_maps ) ) :
				?>

				<div id="service-links-box" class="border-grey">

					<ul class="service-links-list">

						<?php
						if ( ! empty( $website ) ) {
							echo '<a href="' . esc_url( $website ) . '" target="_blank"><li><i class="fa fa-globe" aria-hidden="true"></i>' . esc_html( 'Website', 'horsesales' ) . '</li></a>';
						}

						if ( ! empty( $facebook ) ) {
							echo '<a href="' . esc_url( $facebook ) . '" target="_blank"><li class="li-facebook"><i class="fa fa-facebook" aria-hidden="true"></i>' . esc_html( 'Facebook', 'horsesales' ) . '</li></a>';
						}

						if ( ! empty( $instagram ) ) {
							echo '<a href="' . esc_url( $instagram ) . '" target="_blank"><li class="li-instagram"><i class="fa fa-instagram" aria-hidden="true"></i>' . esc_html( 'Instagram', 'horsesales' ) . '</li></a>';
						}

						if ( ! empty( $twitter ) ) {
							echo '<a href="' . esc_url( $twitter ) . '" target="_blank"><li class="li-twitter"><i class="fa fa-twitter" aria-hidden="true"></i>' . esc_html( 'Twitter', 'horsesales' ) . '</li></a>';
						}

						if ( ! empty( $linkedin ) ) {
							echo '<a href="' . esc_url( $linkedin ) . '" target="_blank"><li class="li-linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i>' . esc_html( 'LinkedIn', 'horsesales' ) . '</li></a>';
						}

						if ( ! empty( $google_maps ) ) {
							echo '<a href="' . esc_url( $google_maps ) . '" target="_blank"><li class="li-google-maps"><i class="fa fa-map" aria-hidden="true"></i>' . esc_html( 'Google Maps', 'horsesales' ) . '</li></a>';
						}

						?>

					</ul>

				</div><!-- /#service-links-box -->

				<?php

			endif;

			?>

			<div id="service-categories-box" class="d-none d-lg-block">

				<div class="desc-heading-wrap">

					<i class="fa fa-tag" aria-hidden="true"></i>

					<h3><?php esc_html_e( 'Categories', 'horsesales' ); ?></h3>

				</div>

				<p><?php echo esc_html( $service_cat ); ?></p>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-12">

			<div id="service-desc-box" class="border-grey">

				<div class="desc-heading-wrap"><i class="fa fa-file-text-o" aria-hidden="true"></i><h2><?php esc_html_e( 'Description', 'horsesales' ); ?></h2></div>

				<div class="service-content"><?php the_content(); ?></div>

			</div>

		</div>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->
