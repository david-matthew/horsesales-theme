<?php
/**
 * Template part for displaying the service listing call-to-action
 *
 * @package Horsesales
 */

?>

<hr>

<div class="row">

	<div class="col-12">

		<p><i class="fa fa-user-plus mr-2" aria-hidden="true"></i><em>Want to get your business listed?</em> <strong><a href="<?php echo esc_url( get_theme_mod( 'mkt_submit_url' ) ); ?>">Click here</a></strong>.</p>

	</div>

</div>
