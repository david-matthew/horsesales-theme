<?php
/**
 * Template part for displaying the product search form
 *
 * @package Horsesales
 */

global $wp_query;
$product_cat    = $wp_query->get_queried_object();
$sub_categories = get_categories(
	array(
		'taxonomy'     => 'product_cat',
		'child_of'     => 0,
		'parent'       => $product_cat->term_id,
		'orderby'      => 'name',
		'show_count'   => 1,
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => true,
	)
);

if ( $sub_categories ) {
	?>

	<form role="search" class="filter" method="get" action="<?php echo esc_url( site_url() ); ?>">

		<div class="form-row">

			<div class="form-group col-8 col-md-6 col-lg-4 col-xl-3 input-group">

				<div class="input-group-prepend">

					<span class="input-group-text"><i class="fa fa-list-ul" aria-hidden="true"></i></span>

				</div>

				<select name="product_cat" id="product-cat" class="form-control">

					<option value="<?php echo esc_html( $product_cat->slug ); ?>"><?php esc_html_e( 'All Categories', 'horsesales' ); ?></option>

					<?php

					foreach ( $sub_categories as $sc ) {
						if ( 'sold' !== $sc->slug ) {
							echo '<option value="' . esc_html( $sc->slug ) . '">' . esc_html( $sc->name ) . '</option>';
						}
					}

					?>

				</select>

			</div>

			<div class="form-group col-4 col-md-2 col-xl-1">

				<input type="hidden" name="post_type" value="product">

				<button type="submit" class="btn btn-primary" value="Search"><i class="fa fa-filter mr-1" aria-hidden="true"></i><?php esc_html_e( 'Filter', 'horsesales' ); ?></button>

			</div>
		</div>

	</form>

	<?php
}
