<?php
/**
 * The page banner template part.
 *
 * @package Horsesales
 */

global $page_banner_img_url;

if ( is_product_category() ) {
	global $wp_query;
	$product_cat         = $wp_query->get_queried_object();
	$thumbnail_id        = get_woocommerce_term_meta( $product_cat->term_id, 'thumbnail_id', true );
	$page_banner_img_url = wp_get_attachment_url( $thumbnail_id );
} else {
	$page_banner_img_url = get_the_post_thumbnail_url( get_the_ID(), 'full' );
}

if ( $page_banner_img_url ) {
	?>
	<div class="page-banner-wrap" data-aos="fade-down">
		<div class="overlay"></div>
		<?php the_title( '<h1 class="entry-title center-absolute">', '</h1>' ); ?>
		<img src="<?php echo esc_url( $page_banner_img_url ); ?>" alt="<?php the_title(); ?>"/>
	</div>
	<?php
}
