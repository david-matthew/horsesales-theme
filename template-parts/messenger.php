<?php
/**
 * The template for displaying the Facebook Messenger Chat widget.
 *
 * @package Horsesales
 */

?>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			xfbml : true,
			version : 'v3.3'
		});
	};
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/en_GB/sdk/xfbml.customerchat.js';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<div class="fb-customerchat"
attribution="setup_tool"
page_id="513095255827684"
theme_color="#207a61"
logged_in_greeting="Welcome to HorseSales.ie! If you have any questions we'll be happy to help."
logged_out_greeting="Welcome to HorseSales.ie! If you have any questions we'll be happy to help."
greeting_dialog_display="hide">
</div>
