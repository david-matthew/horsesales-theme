<?php
/**
 * The breadcrumbs navigation template part.
 *
 * @package Horsesales
 */

if ( function_exists( 'yoast_breadcrumb' ) ) {
	?>

	<div class="container-fluid breadcrumbs-wrapper" data-aos="fade-down">

		<div class="container breadcrumbs"><?php yoast_breadcrumb(); ?></div>

	</div>

	<?php
}
