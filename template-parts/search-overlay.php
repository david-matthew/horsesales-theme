<?php
/**
 * The template part for displaying the search overlay
 *
 * @package Horsesales
 */

?>

<div id="search-overlay">

	<div class="container overlay-content">

		<div id="close-search" class="text-right" title="Close Search">×</div>

		<form role="search" method="get" action="<?php echo esc_url( site_url() ); ?>">

			<label class="screen-reader-text" for="horse-search-field">Search for:</label>

			<input type="search" id="horse-search-field" class="search-field" placeholder="Search Horses…" name="s">

			<button type="submit" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>

			<input type="hidden" name="post_type" value="product">

		</form>

		<div id="quick-searches" class="row">

			<div class="col-xs-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">

				<p>Quick Search by Category:</p>

				<?php

				$horse_categories = get_categories(
					array(
						'taxonomy'     => 'product_cat',
						'orderby'      => 'name',
						'show_count'   => 1,
						'pad_counts'   => 0,
						'hierarchical' => 1,
						'title_li'     => '',
						'hide_empty'   => true,
					)
				);

				foreach ( $horse_categories as $c ) {
					if ( 0 === $c->category_parent && 'Uncategorised' !== $c->name ) {
						$category_id = $c->term_id;
						?>
						<div class="quick-search-link">
							<a href="<?php echo esc_url( get_term_link( $c->slug, 'product_cat' ) ); ?>"><?php echo esc_html( $c->name ); ?></a>
						</div>
						<?php
					}
				}
				?>

			</div>

		</div>

	</div>
	  
</div>
