<?php
/**
 * Template part for displaying results in search pages.
 *
 * @package Horsesales
 */

if ( get_post_type() === 'product' ) {
	get_template_part( 'template-parts/content-product-preview' );
}

if ( get_post_type() === 'service' ) {
	get_template_part( 'template-parts/content-service-preview' );
}
