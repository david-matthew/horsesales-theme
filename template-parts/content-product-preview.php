<?php
/**
 * The template part for displaying product post type
 * preview items/cards (as returned via search).
 *
 * @package Horsesales
 */

global $product;

?>

<div class="col-xs-12 col-md-6 col-lg-4">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="entry-summary">

			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">

				<?php the_post_thumbnail( 'shop_catalog', array( 'class' => 'img-fluid' ) ); ?>

				<h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>

				<?php

				if ( $product->get_type() === 'auction' ) {
					echo '<span class="price"><span class="auction-price">Starting bid: <span class="amount">€' . esc_html( $product->get_curent_bid() ) . '</span></span></span>';
					horsesales_auction_status();
					horsesales_excerpt();
					horsesales_counter();
				} else {
					horsesales_sold_status();
					horsesales_excerpt();
					echo '<a href="' . esc_url( get_permalink() ) . '" class="button">Read More</a>';
				}

				?>

			</a>

		</div>

	</article><!-- #post-<?php the_ID(); ?> -->

</div>
