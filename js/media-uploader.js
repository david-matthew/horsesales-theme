/**
 * Custom Media Upload Functionality.
 *
 * @package Horsesales
 */

jQuery(document).ready(function($){
    
    let mediaUploader;

    function hs_media_upload(buttonID, targetID) {
        $(buttonID).click(function(e) {
            e.preventDefault();
    
            mediaUploader = wp.media.frames.file_frame = wp.media({
                title: 'Choose Image',
                button: {
                    text: 'Choose Image'
                },
                multiple: false
            });
    
            mediaUploader.on('select', function() {
                let attachment = mediaUploader.state().get('selection').first().toJSON();
                $(targetID).val(attachment.url);
            });
    
            mediaUploader.open();
        });
    }

    hs_media_upload('#logo-upload-btn', '#service-logo-url');
    hs_media_upload('#img1-upload-btn', '#service-additional-img-1');
    hs_media_upload('#img2-upload-btn', '#service-additional-img-2');
    hs_media_upload('#img3-upload-btn', '#service-additional-img-3');
    
});