/**
 * General custom JavaScript.
 *
 * This js file handles miscellaneous tasks, such as
 * scroll behaviour, sub-menus, search and library settings.
 *
 * @package Horsesales
 */

// Initialize the AOS library
AOS.init({
    duration: 1200,
    offset: 180
});

jQuery(document).ready(function($) {
    // When the user scrolls down 90px from the top, adjust navbar padding
    window.onscroll = function() {
        if (document.body.scrollTop > 90 || document.documentElement.scrollTop > 90) {
            $('#navbar').css('paddingTop', '10px');
            $('#navbar').css('paddingBottom', '10px');
        } else {
            $('#navbar').css('paddingTop', '20px');
            $('#navbar').css('paddingBottom', '20px'); 
        }
    };

    // Search overlay functions.
    $('.search-btn').on('click', function() {
        $('#search-overlay').css('top', '0');
        //$('#horse-search-field').focus();
    });

    $('#close-search').on('click', function() {
        $('#search-overlay').css('top', '100%');
    });

    // Clicking anywhere outside the submenus will close them.
    $(document).on('click', function() {
        $('.sub-menu').hide('400');
    });

    let nav = $('#primary-menu');

    nav.find('.menu-item-has-children a').on('click', function (e) {

        //Prevent document clicks from auto-closing the submenu.
        e.stopPropagation();

        // Toggle this menu's submenu.
        let submenu = $(this).next('.sub-menu');
        submenu.toggle('400');

        // Close submenus other than this one.
        $('ul.sub-menu', nav).not(submenu).hide('400');

    });
});