/**
 * Woocommerce Customisations.
 * 
 * Front-end customisations to the product page, called only if the post type is a product.
 *
 * @package Horsesales
 */

jQuery(document).ready(function($) {

    // Get the value of the hidden shareable-link field and copy it to the clipboard.
    $('.share-horse').on('click', function() {
        $('input[name=shareable-link').select();
        document.execCommand("copy");
        alert('A shareable link has now been copied to your clipboard. Use it to share on WhatsApp, Facebook, Twitter, email, or wherever you like.');
    });
});