/**
 * Auction Customisations.
 * 
 * Front-end customisations to the auction page, called only when the post is an auction.
 *
 * @package Horsesales
 */

jQuery(document).ready(function($) {
    // Get the auction increment value and make the step equal to it.
    let increment = $('.increment-value').data('increment');
    $('.qty').attr("step", increment);

    // Get the current bid and set the max bid to twice that amount.
    let bid = $('.increment-value').data('current-bid');
    $('.qty').attr("max", bid * 2);
});