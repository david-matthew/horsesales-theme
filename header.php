<?php
/**
 * The header template
 *
 * @package Horsesales
 */

?>
<!doctype html>

<html <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">

		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<?php

		get_template_part( 'template-parts/favicons' );

		get_template_part( 'template-parts/analytics' );

		get_template_part( 'template-parts/fb-pixel' );

		wp_head();

		?>

	</head>

	<body>

	<?php

	get_template_part( 'template-parts/messenger' );

	get_template_part( 'template-parts/search-overlay' );

	get_template_part( 'template-parts/nav-menu' );

	?>
