<?php
/**
 * The file containing the service post type class.
 *
 * @package    Horsesales
 * @subpackage Horsesales/classes
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * The Service class.
 *
 * The class that defines the service post type, its
 * taxonomies and custom meta box/fields.
 *
 * @package    Horsesales
 * @subpackage Horsesales/classes
 */
class Service {

	/**
	 * The class constructor.
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'service_post_type' ), 0 );
		add_action( 'init', array( $this, 'service_category' ), 0 );
		add_action( 'init', array( $this, 'service_location' ), 0 );
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
		add_action( 'save_post', array( $this, 'save_metabox' ) );
	}

	/**
	 * Register the service post type.
	 */
	public function service_post_type() {

		$labels = array(
			'name'                  => _x( 'Services', 'Post Type General Name', 'horsesales' ),
			'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'horsesales' ),
			'menu_name'             => __( 'Services', 'horsesales' ),
			'name_admin_bar'        => __( 'Service', 'horsesales' ),
			'archives'              => __( 'Service Archives', 'horsesales' ),
			'attributes'            => __( 'Service Attributes', 'horsesales' ),
			'parent_item_colon'     => __( 'Parent Item:', 'horsesales' ),
			'all_items'             => __( 'All Services', 'horsesales' ),
			'add_new_item'          => __( 'Add New Service', 'horsesales' ),
			'add_new'               => __( 'Add New', 'horsesales' ),
			'new_item'              => __( 'New Service', 'horsesales' ),
			'edit_item'             => __( 'Edit Service', 'horsesales' ),
			'update_item'           => __( 'Update Service', 'horsesales' ),
			'view_item'             => __( 'View Service', 'horsesales' ),
			'view_items'            => __( 'View Services', 'horsesales' ),
			'search_items'          => __( 'Search Services', 'horsesales' ),
			'not_found'             => __( 'Not found', 'horsesales' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'horsesales' ),
			'featured_image'        => __( 'Featured Image', 'horsesales' ),
			'set_featured_image'    => __( 'Set featured image', 'horsesales' ),
			'remove_featured_image' => __( 'Remove featured image', 'horsesales' ),
			'use_featured_image'    => __( 'Use as featured image', 'horsesales' ),
			'insert_into_item'      => __( 'Insert into service', 'horsesales' ),
			'uploaded_to_this_item' => __( 'Uploaded to this service', 'horsesales' ),
			'items_list'            => __( 'Services list', 'horsesales' ),
			'items_list_navigation' => __( 'Services list navigation', 'horsesales' ),
			'filter_items_list'     => __( 'Filter services list', 'horsesales' ),
		);
		$args = array(
			'label'               => __( 'Services', 'horsesales' ),
			'description'         => __( 'Services and Professions for the HorseSales Marketplace', 'horsesales' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
			'taxonomies'          => array( 'service-category', 'service-tag' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 55,
			'menu_icon'           => 'dashicons-excerpt-view',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		);
		register_post_type( 'service', $args );
	}

	/**
	 * Register the service category capability.
	 */
	public function service_category() {

		$labels = array(
			'name'                       => _x( 'Category', 'Taxonomy General Name', 'horsesales' ),
			'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'horsesales' ),
			'menu_name'                  => __( 'Categories', 'horsesales' ),
			'all_items'                  => __( 'All Categories', 'horsesales' ),
			'parent_item'                => __( 'Parent Category', 'horsesales' ),
			'parent_item_colon'          => __( 'Parent Category:', 'horsesales' ),
			'new_item_name'              => __( 'New Category', 'horsesales' ),
			'add_new_item'               => __( 'Add New Category', 'horsesales' ),
			'edit_item'                  => __( 'Edit Category', 'horsesales' ),
			'update_item'                => __( 'Update Category', 'horsesales' ),
			'view_item'                  => __( 'View Category', 'horsesales' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'horsesales' ),
			'add_or_remove_items'        => __( 'Add or remove categories', 'horsesales' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'horsesales' ),
			'popular_items'              => __( 'Popular Categories', 'horsesales' ),
			'search_items'               => __( 'Search Categories', 'horsesales' ),
			'not_found'                  => __( 'Not Found', 'horsesales' ),
			'no_terms'                   => __( 'No categories', 'horsesales' ),
			'items_list'                 => __( 'Category list', 'horsesales' ),
			'items_list_navigation'      => __( 'Category list navigation', 'horsesales' ),
		);
		$args = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
		);
		register_taxonomy( 'service-category', array( 'service' ), $args );
	}

	/**
	 * Register the service location capability.
	 */
	public function service_location() {

		$labels = array(
			'name'                       => _x( 'Location', 'Taxonomy General Name', 'horsesales' ),
			'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'horsesales' ),
			'menu_name'                  => __( 'Locations', 'horsesales' ),
			'all_items'                  => __( 'All Locations', 'horsesales' ),
			'parent_item'                => __( 'Parent Location', 'horsesales' ),
			'parent_item_colon'          => __( 'Parent Location:', 'horsesales' ),
			'new_item_name'              => __( 'New Location', 'horsesales' ),
			'add_new_item'               => __( 'Add New Location', 'horsesales' ),
			'edit_item'                  => __( 'Edit Location', 'horsesales' ),
			'update_item'                => __( 'Update Location', 'horsesales' ),
			'view_item'                  => __( 'View Location', 'horsesales' ),
			'separate_items_with_commas' => __( 'Separate locations with commas', 'horsesales' ),
			'add_or_remove_items'        => __( 'Add or remove locations', 'horsesales' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'horsesales' ),
			'popular_items'              => __( 'Popular Locations', 'horsesales' ),
			'search_items'               => __( 'Search Locations', 'horsesales' ),
			'not_found'                  => __( 'Not Found', 'horsesales' ),
			'no_terms'                   => __( 'No Locations', 'horsesales' ),
			'items_list'                 => __( 'Location list', 'horsesales' ),
			'items_list_navigation'      => __( 'Location list navigation', 'horsesales' ),
		);
		$args = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
		);
		register_taxonomy( 'service-location', array( 'service' ), $args );
	}

	/**
	 * Add the metabox.
	 */
	public function add_metabox() {
		add_meta_box(
			'hs_service_fields',
			'Service Custom Fields',
			array( $this, 'create_metabox' ),
			'service',
			'normal',
			'default'
		);
	}

	/**
	 * Create the metabox custom fields.
	 *
	 * @param object $post The post object.
	 */
	public function create_metabox( $post ) {

		wp_nonce_field( basename( __FILE__ ), 'hs_service_nonce' );
		$hs_services_meta = get_post_meta( $post->ID );

		?>

		<div class="service-fields">

			<div class="form-group">

				<span class="dashicons dashicons-admin-links"></span>

				<label for="service-website"><?php esc_html_e( 'Website Link', 'horsesales' ); ?></label>

				<input type="url" name="service_website" id="service-website" value="<?php if ( ! empty( $hs_services_meta['service_website'][0] ) ) echo esc_url( $hs_services_meta['service_website'][0] ); ?>">

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-phone"></span>

				<label for="service-phn"><?php esc_html_e( 'Contact Number', 'horsesales' ); ?></label>

				<input type="tel" name="service_phn" id="service-phn" value="<?php if ( ! empty( $hs_services_meta['service_phn'] ) ) echo esc_html( $hs_services_meta['service_phn'][0] ); ?>" required>

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-email-alt"></span>

				<label for="service-email"><?php esc_html_e( 'Email', 'horsesales' ); ?></label>

				<input type="email" name="service_email" id="service-email" value="<?php if ( ! empty( $hs_services_meta['service_email'] ) ) echo esc_html( $hs_services_meta['service_email'][0] ); ?>" required>

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-location"></span>

				<label for="service-address"><?php esc_html_e( 'Address', 'horsesales' ); ?></label>

				<input type="text" name="service_address" id="service-address" value="<?php if ( ! empty( $hs_services_meta['service_address'][0] ) ) echo esc_html( $hs_services_meta['service_address'][0] ); ?>">

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-location-alt"></span>

				<label for="service-gmaps"><?php esc_html_e( 'Google Maps', 'horsesales' ); ?></label>

				<input type="url" name="service_gmaps" id="service-gmaps" value="<?php if ( ! empty( $hs_services_meta['service_gmaps'][0] ) ) echo esc_html( $hs_services_meta['service_gmaps'][0] ); ?>">

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-facebook"></span>

				<label for="service-facebook"><?php esc_html_e( 'Facebook', 'horsesales' ); ?></label>

				<input type="url" name="service_facebook" id="service-facebook" value="<?php if ( ! empty( $hs_services_meta['service_facebook'][0] ) ) echo esc_html( $hs_services_meta['service_facebook'][0] ); ?>">

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-instagram"></span>

				<label for="service-instagram"><?php esc_html_e( 'Instagram', 'horsesales' ); ?></label>

				<input type="url" name="service_instagram" id="service-instagram" value="<?php if ( ! empty( $hs_services_meta['service_instagram'][0] ) ) echo esc_html( $hs_services_meta['service_instagram'][0] ); ?>">

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-twitter"></span>

				<label for="service-twitter"><?php esc_html_e( 'Twitter', 'horsesales' ); ?></label>

				<input type="url" name="service_twitter" id="service-twitter" value="<?php if ( ! empty( $hs_services_meta['service_twitter'][0] ) ) echo esc_html( $hs_services_meta['service_twitter'][0] ); ?>">

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-businessman"></span>

				<label for="service-linkedin"><?php esc_html_e( 'LinkedIn', 'horsesales' ); ?></label>

				<input type="url" name="service_linkedin" id="service-linkedin" value="<?php if ( ! empty( $hs_services_meta['service_linkedin'][0] ) ) echo esc_html( $hs_services_meta['service_linkedin'][0] ); ?>">

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-video-alt3"></span>

				<label for="service-video-id"><?php esc_html_e( 'YouTube Video ID', 'horsesales' ); ?></label>

				<input type="text" name="service_video_id" id="service-video-id" value="<?php if ( ! empty( $hs_services_meta['service_video_id'][0] ) ) echo esc_html( $hs_services_meta['service_video_id'][0] ); ?>">

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-format-image"></span>

				<label for="service-logo-url"><?php esc_html_e( 'Logo / Thumbnail', 'horsesales' ); ?></label>

				<input type="url" name="service_logo_url" id="service-logo-url" value="<?php if ( ! empty( $hs_services_meta['service_logo_url'] ) ) echo esc_url( $hs_services_meta['service_logo_url'][0] ); ?>">

				<button id="logo-upload-btn" class="button-primary"><?php esc_html_e( 'Insert Image', 'horsesales' ); ?></button>

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-format-image"></span>

				<label for="service-additional-img-1"><?php esc_html_e( 'Additional Image 1', 'horsesales' ); ?></label>

				<input type="url" name="service_additional_img_1" id="service-additional-img-1" value="<?php if ( ! empty( $hs_services_meta['service_additional_img_1'] ) ) echo esc_url( $hs_services_meta['service_additional_img_1'][0] ); ?>">

				<button id="img1-upload-btn" class="button-primary"><?php esc_html_e( 'Insert Image', 'horsesales' ); ?></button>

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-format-image"></span>

				<label for="service-additional-img-2"><?php esc_html_e( 'Additional Image 2', 'horsesales' ); ?></label>

				<input type="url" name="service_additional_img_2" id="service-additional-img-2" value="<?php if ( ! empty( $hs_services_meta['service_additional_img_2'] ) ) echo esc_url( $hs_services_meta['service_additional_img_2'][0] ); ?>">

				<button id="img2-upload-btn" class="button-primary"><?php esc_html_e( 'Insert Image', 'horsesales' ); ?></button>

			</div>

			<div class="form-group">

				<span class="dashicons dashicons-format-image"></span>

				<label for="service-additional-img-3"><?php esc_html_e( 'Additional Image 3', 'horsesales' ); ?></label>

				<input type="url" name="service_additional_img_3" id="service-additional-img-3" value="<?php if ( ! empty( $hs_services_meta['service_additional_img_3'] ) ) echo esc_url( $hs_services_meta['service_additional_img_3'][0] ); ?>">

				<button id="img3-upload-btn" class="button-primary"><?php esc_html_e( 'Insert Image', 'horsesales' ); ?></button>

			</div>

		</div>	

		<?php
	}

	/**
	 * The metabox save function.
	 *
	 * @param string $post_id The post ID.
	 */
	public function save_metabox( $post_id ) {
		$is_autosave    = wp_is_post_autosave( $post_id );
		$is_revision    = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST['hs_service_nonce'] ) && wp_verify_nonce( $_POST['hs_service_nonce'], basename( __FILE__ ) ) ) ? 'true' : 'false';

		if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
			return;
		}

		if ( isset( $_POST['service_website'] ) ) {
			update_post_meta( $post_id, 'service_website', esc_url_raw( wp_unslash( $_POST['service_website'] ) ) );
		}

		if ( isset( $_POST['service_phn'] ) ) {
			update_post_meta( $post_id, 'service_phn', sanitize_text_field( wp_unslash( $_POST['service_phn'] ) ) );
		}

		if ( isset( $_POST['service_email'] ) ) {
			update_post_meta( $post_id, 'service_email', sanitize_text_field( wp_unslash( $_POST['service_email'] ) ) );
		}

		if ( isset( $_POST['service_address'] ) ) {
			update_post_meta( $post_id, 'service_address', sanitize_text_field( wp_unslash( $_POST['service_address'] ) ) );
		}

		if ( isset( $_POST['service_gmaps'] ) ) {
			update_post_meta( $post_id, 'service_gmaps', esc_url_raw( wp_unslash( $_POST['service_gmaps'] ) ) );
		}

		if ( isset( $_POST['service_facebook'] ) ) {
			update_post_meta( $post_id, 'service_facebook', esc_url_raw( wp_unslash( $_POST['service_facebook'] ) ) );
		}

		if ( isset( $_POST['service_instagram'] ) ) {
			update_post_meta( $post_id, 'service_instagram', esc_url_raw( wp_unslash( $_POST['service_instagram'] ) ) );
		}

		if ( isset( $_POST['service_twitter'] ) ) {
			update_post_meta( $post_id, 'service_twitter', esc_url_raw( wp_unslash( $_POST['service_twitter'] ) ) );
		}

		if ( isset( $_POST['service_linkedin'] ) ) {
			update_post_meta( $post_id, 'service_linkedin', esc_url_raw( wp_unslash( $_POST['service_linkedin'] ) ) );
		}

		if ( isset( $_POST['service_logo_url'] ) ) {
			update_post_meta( $post_id, 'service_logo_url', esc_url_raw( wp_unslash( $_POST['service_logo_url'] ) ) );
		}

		if ( isset( $_POST['service_additional_img_1'] ) ) {
			update_post_meta( $post_id, 'service_additional_img_1', esc_url_raw( wp_unslash( $_POST['service_additional_img_1'] ) ) );
		}

		if ( isset( $_POST['service_additional_img_2'] ) ) {
			update_post_meta( $post_id, 'service_additional_img_2', esc_url_raw( wp_unslash( $_POST['service_additional_img_2'] ) ) );
		}

		if ( isset( $_POST['service_additional_img_3'] ) ) {
			update_post_meta( $post_id, 'service_additional_img_3', esc_url_raw( wp_unslash( $_POST['service_additional_img_3'] ) ) );
		}

		if ( isset( $_POST['service_video_id'] ) ) {
			update_post_meta( $post_id, 'service_video_id', sanitize_text_field( wp_unslash( $_POST['service_video_id'] ) ) );
		}
	}
}
